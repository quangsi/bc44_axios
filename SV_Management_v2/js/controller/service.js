var svService = {
  getList: function () {
    return axios({
      url: "https://633ec05b0dbc3309f3bc5455.mockapi.io/sv",
      method: "GET",
    });
  },

  remove: function (id) {
    return axios({
      url: `https://633ec05b0dbc3309f3bc5455.mockapi.io/sv/${id}`,
      method: "DELETE",
    });
  },
  create: function (data) {
    return axios({
      url: BASE_URL,
      method: "POST",
      data: data,
    });
  },
};

// chạy 1
// ko chạy 0
