// then() khi gọi api thành công
// catch() khi gọi api thất bại

// lấy dữ liệu từ server và render

const BASE_URL = "https://633ec05b0dbc3309f3bc5455.mockapi.io/sv";
var idSelected = null;
function fetchDSSV() {
  batLoading();
  svService
    .getList()
    .then(function (res) {
      tatLoading();
      // gọi function renderDSSV trong then() => nếu gọi ngoài then sẽ ko có dữ liệu đem đi render
      console.log("🚀 - file: demo.js:8 - res", res.data);
      renderDSSV(res.data.reverse());
    })
    .catch(function (err) {
      tatLoading();
      console.log("🚀 - file: demo.js:12 - err", err);
    });
}
fetchDSSV();
// xoá sv ( trên server)
function xoaSV(id) {
  console.log(id);
  batLoading();
  svService
    .remove(id)
    .then(function (res) {
      fetchDSSV();
      console.log(res);
      Toastify({
        text: "Xoá sinh viên thành công",
        offset: {
          x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
          y: 10, // vertical axis - can be a number or a string indicating unity. eg: '2em'
        },
      }).showToast();
      tatLoading();
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}
function themSinhVien() {
  console.log("yes");
  var dataSv = layThongTinTuFrom();
  console.log("🚀 - file: main.js:46 - dataSv", dataSv);

  svService
    .create(dataSv)
    .then(function (res) {
      // gọi lại api lấy danh sách mới nhất từ server sau khi xoá thành công
      fetchDSSV();
      console.log(res);
    })
    .catch(function (err) {
      console.log(err);
    });
}

function suaSV(id) {
  idSelected = id;
  batLoading();
  axios({
    url: `https://633ec05b0dbc3309f3bc5455.mockapi.io/sv/${id}`,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();
      showThongTinLenForm(res.data);
      console.log(res);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}

function capNhatSinhVien() {
  axios({
    url: `${BASE_URL}/${idSelected}`,
    method: "PUT",
    data: layThongTinTuFrom(),
  })
    .then(function (res) {
      fetchDSSV();
      console.log(res);
    })
    .catch(function (err) {
      console.log(err);
    });
}
