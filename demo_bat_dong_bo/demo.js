//  đồng bộ : chạy theo thứ tự từ trên xuống dưới

//  bất đồng bộ : gọi api, setTimeOut
setTimeout(function () {
  console.log(0);
}, 1000);

console.log(1);
console.log(2);
console.log(3);
// setTimeout ~ delay
setTimeout(function () {
  console.log("hello user");
}, 5000);
// 3000 ~ 3s

console.log(4);

// sẽ chạy hết code đồng bộ, rồi chuyển sang code bất đồng bộ chạy
